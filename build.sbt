ThisBuild / scalaVersion     := "2.12.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "scala-steward-test",
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-effect" % "1.0.0",
      "org.typelevel" %% "cats-core" % "1.0.0",
      "com.github.daddykotex" %% "courier" % "1.0.0-RC1"
    )
  )
